Feature: Purchasing on the Store

  Scenario: An user can order a laptop
  	Given I am on the Home page
  	When I click on the category "<category>"
  	And I click on the laptop1 "<laptop1>"
  	And I click on the button Add to cart
  	And I click on the Accept alert
  	Then The laptop1 "<laptop1>" should be added to the cart
  	When I navigate again to the category "<category>"
  	And I click on the laptop2 "<laptop2>"
  	And I click on the button "Add to cart"
  	And I click on the Accept alert for the laptop2
  	Then The second laptop "<laptop2>" should be added to the cart
  	When I click on the button to delete the laptop "<laptop2>"
  	Then The second laptop is removed from the cart and Total is <priceLaptop1>
  	When I click on the button Place order
  	Then It appears a form to fill with the user data "<name>" "<country>" "<city>" <creditCard> <month> <year>
  	When I click on the button Purchase
  	Then It appears a confirmation message with the text "Thank you for your purchase!"
  	And It also appears the Order ID and the amount of the laptop <priceLaptop1>
  	And I press the Ok button to close the message
  	
  	Examples: 
      | category	|	laptop1				|	laptop2			|	priceLaptop1	|	name			|	country	|	city		|	creditCard	|	month	|	year	|
      | Laptops 	| Sony vaio i5	|	Dell i7 8gb	|	790						|	John Doe	|	Germany	|	Berlin	|	12345				|	05		|	2022	|

      