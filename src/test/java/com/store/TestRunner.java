package com.store;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features= {"src/test/resources/features"},
					plugin= {
						//"pretty",
						"html:results/html/cucumber.html"	
					},
					monochrome=true)

public class TestRunner {

}
