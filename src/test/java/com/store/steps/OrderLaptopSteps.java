package com.store.steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.helpers.LoggerHelper;
import com.helpers.WaitHelper;
import com.store.pageObjects.StorePage;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class OrderLaptopSteps {

	WebDriver driver;
	StorePage storePage;
	WaitHelper waitHelper;

	Logger log = LoggerHelper.getLogger(OrderLaptopSteps.class);

	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\src\\test\\resources\\drivers\\chromedriver.exe");

		HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("profile.default_content_setting_values.notifications", 2);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", map);
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();

		storePage = new StorePage(this.driver);
		waitHelper = new WaitHelper(this.driver);
	}

	@Given("I am on the Home page")
	public void i_am_on_the_home_page() {
		
		log.info("Store App - Order a laptop ----- STARTED");

		driver.get("https://www.demoblaze.com/index.html");

		waitHelper.WaitForElement(storePage.getLaptopsLink());
		storePage.getLaptopsLink().isDisplayed();
	}

	@When("I click on the category {string}")
	public void i_click_on_the_category(String category) {

		log.info("Click on the category: " + category);

		storePage.getLaptopsLink().click();
	}

	@And("I click on the laptop1 {string}")
	public void i_click_on_the_laptop1(String laptop1) {

		log.info("Click on the laptop1: " + laptop1);

		waitHelper.WaitForElement(storePage.getLaptopSonyVaioI5());
		storePage.getLaptopSonyVaioI5().click();
	}

	@And("I click on the button Add to cart")
	public void i_click_on_the_button_add_to_cart() {

		log.info("Click on the button 'Add to cart'");

		waitHelper.WaitForElement(storePage.getAddToCartLink());
		storePage.getAddToCartLink().click();
	}

	@And("I click on the Accept alert")
	public void i_click_on_the_accept_alert() {

		log.info("Click on the Accept alert button");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.switchTo().alert().accept();
	}

	@Then("The laptop1 {string} should be added to the cart")
	public void the_laptop1_should_be_added_to_the_cart(String laptop1) {

		log.info("Check the " + laptop1 + " was added to the cart");

		storePage.getCartLink().click();

		waitHelper.WaitForElement(storePage.getTextCartLaptopSonyVaioI5());
		storePage.getTextCartLaptopSonyVaioI5().isDisplayed();
	}

	@When("I navigate again to the category {string}")
	public void i_navigate_again_to_the_category(String category) {

		log.info("Navigate to the categoy: " + category);

		storePage.getHomeLink().click();
		storePage.getLaptopsLink().click();
	}

	@And("I click on the laptop2 {string}")
	public void i_click_on_the_laptop2(String laptop2) {

		log.info("Click on the laptop2: " + laptop2);

		waitHelper.WaitForElement(storePage.getLaptopDellI7());
		storePage.getLaptopDellI7().click();
	}

	@And("I click on the button {string}")
	public void i_click_on_the_button(String button) {

		log.info("Click on the button: " + button);

		waitHelper.WaitForElement(storePage.getAddToCartLink());
		storePage.getAddToCartLink().click();
	}

	@And("I click on the Accept alert for the laptop2")
	public void i_click_on_the_accept_alert_for_the_laptop2() {

		log.info("Click on the Accept alert button");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.switchTo().alert().accept();
	}

	@Then("The second laptop {string} should be added to the cart")
	public void the_second_laptop_should_be_added_to_the_cart(String laptop2) {

		log.info("Check the " + laptop2 + " was added to the cart");

		storePage.getCartLink().click();

		waitHelper.WaitForElement(storePage.getTextCartLaptopDellI7());
		storePage.getTextCartLaptopDellI7().isDisplayed();
	}

	@When("I click on the button to delete the laptop {string}")
	public void i_click_on_the_button_to_delete_the_laptop(String laptop2) {

		log.info("Delete the " + laptop2 + " from the cart");

		storePage.getDeleteLinkLaptopDellI7().click();
	}

	@Then("The second laptop is removed from the cart and Total is {int}")
	public void the_second_laptop_is_removed_from_the_cart(Integer total) {

		log.info("Check that Total is: " + total);

		waitHelper.WaitForElement(storePage.getCartTotal());
		storePage.getCartTotal().isDisplayed();
	}

	@When("I click on the button Place order")
	public void i_click_on_the_button_place_order() {

		log.info("Click on the button Place order");

		storePage.getButtonPlaceOrder().click();
	}

	@Then("It appears a form to fill with the user data {string} {string} {string} {int} {int} {int}")
	public void it_appears_a_form_to_fill_with_the_user_data(String name, String country, String city,
			Integer creditCard, Integer month, Integer year) {

		log.info("Fill the form for placing the order");

		waitHelper.WaitForElement(storePage.getButtonPurchase());

		storePage.getFieldName().clear();
		storePage.getFieldName().sendKeys(name);

		storePage.getFieldCountry().clear();
		storePage.getFieldCountry().sendKeys(country);

		storePage.getFieldCity().clear();
		storePage.getFieldCity().sendKeys(city);

		storePage.getFieldCard().clear();
		storePage.getFieldCard().sendKeys(creditCard.toString());

		storePage.getFieldMonth().clear();
		storePage.getFieldMonth().sendKeys(month.toString());

		storePage.getFieldYear().clear();
		storePage.getFieldYear().sendKeys(year.toString());

	}

	@When("I click on the button Purchase")
	public void i_click_on_the_button_purchase() {

		log.info("Click on the button Purchase");

		storePage.getButtonPurchase().click();
	}

	@Then("It appears a confirmation message with the text {string}")
	public void it_appears_a_confirmation_message_with_the_text(String purchaseOk) {

		log.info("Purchase confirmed with message: " + purchaseOk);

		waitHelper.WaitForElement(storePage.getButtonOk());
		storePage.getPurchaseOkMessage().isDisplayed();
	}

	@And("It also appears the Order ID and the amount of the laptop {int}")
	public void it_also_appears_the_order_id_and_the_amount_of_the_laptop(int priceLaptop1) {

		log.info("Check the Order ID and the price must be equal to the price of laptop1: " + priceLaptop1);

		String orderInfo = storePage.getOrderInfo().getText();
		String[] info = orderInfo.split(":");		
		String id = info[1].trim().split("\\s+")[0];
		String amount = info[2].trim().split("\\s+")[0];
		
		assertTrue(Integer.parseInt(id) > 0);
		assertEquals(priceLaptop1, Integer.parseInt(amount));
		
		log.info("The Order ID is: "+id+". And the Order amount is: "+amount+".");
	}
	
	@And("I press the Ok button to close the message")
	public void i_press_the_ok_button_to_close_the_message() {

		log.info("Press the Ok button to close the Order info message");

		storePage.getButtonOk().click();
		
		log.info("Store App - Order a laptop ----- FINISHED\n\n\n");
	}
	
	@After
	public void end(){
		driver.quit();
	}
}
