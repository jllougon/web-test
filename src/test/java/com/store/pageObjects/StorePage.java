package com.store.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StorePage {

	WebDriver driver;

	@FindBy(xpath = "//a[@id='nava']")
	WebElement homeLink;

	@FindBy(xpath = "//a[text()='Laptops']")
	WebElement laptopsLink;

	@FindBy(xpath = "//a[text()='Sony vaio i5']")
	WebElement laptopSonyVaioI5;

	@FindBy(xpath = "//a[text()='Dell i7 8gb']")
	WebElement laptopDellI7;

	@FindBy(xpath = "//a[text()='Cart']")
	WebElement cartLink;

	@FindBy(xpath = "//a[text()='Add to cart']")
	WebElement addToCartLink;

	@FindBy(xpath = "//td[text()='Sony vaio i5']")
	WebElement textCartLaptopSonyVaioI5;

	@FindBy(xpath = "//td[text()='Dell i7 8gb']")
	WebElement textCartLaptopDellI7;

	@FindBy(xpath = "//td[text()='Dell i7 8gb']/following-sibling::td[2]/a[text()='Delete']")
	WebElement deleteLinkLaptopDellI7;

	@FindBy(xpath = "//h3[@id='totalp'][text()='790']")
	WebElement cartTotal;

	@FindBy(xpath = "//button[text()='Place Order']")
	WebElement buttonPlaceOrder;

	@FindBy(xpath = "//button[text()='Purchase']")
	WebElement buttonPurchase;

	@FindBy(xpath = "//input[@id='name']")
	WebElement fieldName;

	@FindBy(xpath = "//input[@id='country']")
	WebElement fieldCountry;

	@FindBy(xpath = "//input[@id='city']")
	WebElement fieldCity;

	@FindBy(xpath = "//input[@id='card']")
	WebElement fieldCard;

	@FindBy(xpath = "//input[@id='month']")
	WebElement fieldMonth;

	@FindBy(xpath = "//input[@id='year']")
	WebElement fieldYear;
	
	@FindBy(xpath = "//h2[text()='Thank you for your purchase!']")
	WebElement purchaseOkMessage;
	
	@FindBy(xpath = "//h2/following-sibling::p[contains(text(),'')]")
	WebElement orderInfo;
	
	@FindBy(xpath = "//button[text()='OK']")
	WebElement buttonOk;

	public StorePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public WebElement getHomeLink() {
		return homeLink;
	}

	public WebElement getLaptopsLink() {
		return laptopsLink;
	}

	public WebElement getLaptopSonyVaioI5() {
		return laptopSonyVaioI5;
	}

	public WebElement getLaptopDellI7() {
		return laptopDellI7;
	}

	public WebElement getCartLink() {
		return cartLink;
	}

	public WebElement getAddToCartLink() {
		return addToCartLink;
	}

	public WebElement getTextCartLaptopSonyVaioI5() {
		return textCartLaptopSonyVaioI5;
	}

	public WebElement getTextCartLaptopDellI7() {
		return textCartLaptopDellI7;
	}

	public WebElement getDeleteLinkLaptopDellI7() {
		return deleteLinkLaptopDellI7;
	}
	
	public WebElement getCartTotal() {
		return cartTotal;
	}

	public WebElement getButtonPlaceOrder() {
		return buttonPlaceOrder;
	}
	
	public WebElement getButtonPurchase() {
		return buttonPurchase;
	}

	public WebElement getFieldName() {
		return fieldName;
	}

	public WebElement getFieldCountry() {
		return fieldCountry;
	}

	public WebElement getFieldCity() {
		return fieldCity;
	}

	public WebElement getFieldCard() {
		return fieldCard;
	}

	public WebElement getFieldMonth() {
		return fieldMonth;
	}

	public WebElement getFieldYear() {
		return fieldYear;
	}

	public WebElement getPurchaseOkMessage() {
		return purchaseOkMessage;
	}

	public WebElement getOrderInfo() {
		return orderInfo;
	}

	public WebElement getButtonOk() {
		return buttonOk;
	}
}
